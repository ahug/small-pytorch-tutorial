TODO:
- Check out https://mybinder.org/
- Check out https://github.com/damianavila/RISE

TIPS from Sebastian:
- Introduction got a bit lost, we should first motivate PyTorch:
  - What are alternatives, what are similarities?
  - _Why do we need tensors? What is the difference to normal numpy-arrays? Maybe start with a computation graph and show all the attributes which tensors have (grad, grad_fn, requires_grad, etc.)_


## What is PyTorch
- Numpy extension with GPU support
- Deep Learning functionality
- etc.

## Basic Tensor Operations (Evann)
- Creating Tensors, dtype, size, etc.
- Operations, Slicing
- Mutating Functions (add_(), etc.)
- converting to/from numpy (.numpy(), torch.from_numpy())
- CUDA (.is_available(), device, etc.)

## Auto-Differentiation (Evann)
- requires_grad, backward(), .grad
- detach()
- with torch.no_grad()
- example how to do backward pass
- basic idea of how auto-diff works/is implemented

## Optimization (Evann)
- visualization of different optimizers
- gradient accumulation
- higher degree derivatives
- different optimization algos
- data parallel

## Neural Network (Andreas)
- e.g. simple CNN
- Loss Functions
- Optimizers (learning rate scheduler?)

## Classifier (Andreas)
- e.g. using the previously introduced CNN
- Cifar10 / MNNIST
- LSTM where we feed pixels line-by-line + illustration where we show the prediction online

## Autoencoder (Evann)
- e.g. on MNIST

## Transfer Learning (Andreas)
- show how PyTorch models can be loaded/stored and reused


## Other Ideas
- TensorboardX?
- Logging in general / mlflow
- Time series with live prediction visualization
- What differentiates PyTorch from Tensorflow/Keras/etc.
